/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

/**

@page TrigNavTools_page TrigNavTools package

@section TrigNavTools_TrigNavToolsIntro Introduction

  This package contains tools and algorithms for performing slimming of the 
  trigger Navigation structure:
  - HLT::TrigNavigationSlimming is an algorithm for performing slimming
  - HLT::TrigNavigationSlimmingTool is a tool for performing slimming

  To use the TrigNavigationSlimming algorith, please use the topOptions in the
  share directory

*/
